package com.company;


import com.sun.org.apache.xpath.internal.functions.Function;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.function.BiFunction;

/**
 * Created by taghawi on 10/21/16.
 */
public class Contract {
    String name = new String();

    public void Contract(String name) {
        this.name = name;
    }
    public Double valutation(Map<Object, Object> parameters,  Map<Contract, BiFunction<Contract, Map, Double>> value_functions) {
        return value_functions.get(this.getClass()).apply(this, parameters);
    }

    public Double valutation(Map<Object, Object> parameters,  BiFunction<Contract, Map, Double> value_function) {
        return value_function.apply(this, parameters);
    }
}
