package com.company;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiFunction;



public class Main {

    public static void main(String[] args) throws Exception {
        Inventory inventory = new Inventory();
        System.out.println("add cookies");
        Good cookies = new Good("cookies", 5);
        inventory.add(cookies);
        System.out.println(inventory.goods);
        System.out.println("add more cookies");
        Good cookies2 = new Good("cookies", 5);
        inventory.add(cookies2);
        System.out.println(inventory.goods);
        System.out.println("remove 5 cookies");
        inventory.remove(cookies);
        System.out.println(inventory.goods);
        inventory.add(new PullOption("po", 5, "intel"));
        System.out.println(inventory.contracts);
        Bond bond1 = new Bond("bond1", 0, 10, 1, 10);
        inventory.add(bond1);
        inventory.add(new Bond("bond2", 0, 100, -1, -100));
        System.out.println(inventory.contracts);

        Map<Object, Object> scenario = new HashMap<Object, Object>();
        scenario.put("price_cookies", 1.0);
        scenario.put("price_intel", 7.0);
        scenario.put("interestrate", 0.01);

        HashMap<Class<?>, BiFunction<Contract, Map, Double>> usgap = Valutation.usgap();

        System.out.println("assets");
        System.out.println(inventory.assets(scenario, usgap));


        System.out.println("liablities");
        System.out.println(inventory.liabilities(scenario, usgap));


        System.out.println("assets");
        System.out.println(inventory.assets(scenario, usgap));

        System.out.println("assetvalue");
        System.out.println("pay one out of two bonds");
        System.out.println(inventory.asset_value(scenario, usgap));
        System.out.println("valutation of bond1");
        //System.out.println(bond1.valutation(scenario, usgap));
        bond1.pay();
        System.out.println("bond and asset value after bond1 is payed");
        //System.out.println(bond1.valutation(scenario, usgap.get(Bond)));
        System.out.println(inventory.asset_value(scenario, usgap));
    }
}
