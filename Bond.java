package com.company;

/**
 * Created by taghawi on 11/3/16.
 */
public class Bond extends Contract {
    int state;
    int num_payments;
    double value_payments;
    double value_maturity;

    public Bond(String name, int state, int num_payments, double value_payments, double value_maturity) {
        this.name = name;
        this.state = state;
        this.num_payments = num_payments;
        this.value_payments = value_payments;
        this.value_maturity = value_maturity;
    }

    public double pay() {
        if (state == num_payments) {
            return 0;
        } else {
            state += 1;
            if (state == num_payments) {
                return value_maturity + value_payments;
            } else {
                return value_payments;
            }
        }
    }
}


