package com.company;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

public class Valutation {
    public static HashMap<Class<?>, BiFunction<Contract, Map, Double>> usgap() {
        BiFunction<Contract, Map, Double> value_bond = (contract, parameters) -> {
            Bond bond = (Bond)contract;
            double i = (Double)parameters.get("interestrate");
            int N = bond.num_payments - bond.state;
            return bond.value_payments * ((1 - (Math.pow((1 + i), (-N)))) / i) + bond.value_maturity * Math.pow((1 + i), (-N));
        };
        BiFunction<Contract, Map, Double> value_pulloption = (contract, parameters) -> {
            PullOption po = (PullOption)contract;
            return Math.max(0, (Double)parameters.get("price_" + po.underlying) - po.execution_price);
        };
        HashMap<Class<?>, BiFunction<Contract, Map, Double>> accounting;
        accounting = new HashMap<Class<?>, BiFunction<Contract, Map, Double>>();
        accounting.put(Bond.class, value_bond);
        accounting.put(PullOption.class, value_pulloption);
        return accounting;
    }
}
