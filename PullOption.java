package com.company;

/**
 * Created by taghawi on 11/3/16.
 */
public class PullOption extends Contract {
    public final double execution_price;
    public final String underlying;
    int state;
    int num_payments;
    double value_payments;
    double value_maturity;

    public PullOption(String name, double execution_price, String underlying) {
        this.name = name;
        this.execution_price = execution_price;
        this.underlying = underlying;
    }

    public double execute(double underlying_price) {
        return Math.max(0, underlying_price - this.execution_price);
    }
}
