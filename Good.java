package com.company;

import java.util.Map;
import java.util.function.BiFunction;

/**
 * Created by taghawi on 10/21/16.
 */
public class Good extends Contract {
    String name = new String();
    double quantity;

    public Good(String type, double quantity) {
        this.name = type;
        this.quantity = quantity;
    }

    public Double valutation(Map<Object, Object> parameters, Map<Contract, BiFunction<Contract, Map, Double>> value_functions) {
        return (Double) parameters.get("price_" + name) * quantity;
    }
    public Double valutation(Map<Object, Object> parameters, BiFunction<Contract, Map, Double> value_function) {
        return (Double) parameters.get("price_" + name) * quantity;
    }
}